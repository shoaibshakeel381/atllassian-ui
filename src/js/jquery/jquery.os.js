jQuery.os = {};
(function () {
    var platform = navigator.platform.toLowerCase();
    jQuery.os.windows = (platform.indexOf('win') != -1);
    jQuery.os.mac = (platform.indexOf('mac') != -1);
    jQuery.os.linux = (platform.indexOf('linux') != -1);

    AJS.deprecate.prop(jQuery, 'os', {
        removeInVersion: '6.0.0',
        displayName: 'jQuery.os',
        extraInfo: 'See https://ecosystem.atlassian.net/browse/AUI-3863.'
    });
}());
