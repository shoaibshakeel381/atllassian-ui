'use strict';

var mac = require('mac');
var libJs = require('./lib/js');
var libLess = require('./lib/less');

module.exports = mac.parallel(
    libJs,
    libLess
);
