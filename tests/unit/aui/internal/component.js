import component from '../../../../src/js/aui/internal/component';

describe('aui/internal/component', function () {
    var Componentised = component('component-name', {});

    it('global AJS.componentName', function () {
        expect(AJS.element.componentName).to.equal(Componentised);
    });

    it('element name prefix "aui-"', function () {
        // TODO This should change to be a function call when using Skate 0.14+.
        // This also means that the variable should be lowercased then.
        expect(new Componentised().tagName).to.equal('AUI-COMPONENT-NAME');
    });
});
