'use strict';

var pkg = require('../../package.json');

module.exports = function (code) {
    return code.toString().replace(/\$\{project\.version\}/g, pkg.version);
};
