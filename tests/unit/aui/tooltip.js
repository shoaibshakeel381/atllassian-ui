'use strict';

import '../../../src/js/aui/tooltip';
import $ from '../../../src/js/aui/jquery';
import helpers from '../../helpers/all';

describe('aui/tooltip', function () {
    const TIPSY_DELAY = 10;

    let clock;
    let $el;
    beforeEach(function () {
        clock = sinon.useFakeTimers();
    });

    afterEach(function () {
        clock.restore();
    });

    function visibleTipsies () {
        return $(document.body).find('.tipsy').filter(':visible');
    }

    function runSharedTooltipTests () {
        it('will show up when delayed', function () {
            expect(visibleTipsies().length).to.equal(0);

            helpers.hover($el);

            clock.tick(1);
            expect(visibleTipsies().length).to.equal(0);

            clock.tick(TIPSY_DELAY);
            const $tipsies = visibleTipsies();
            expect($tipsies.length).to.equal(1);

            const position = $tipsies.position();
            expect(position.left).to.not.equal(0);
            expect(position.top).to.not.equal(0);
        });

        it('will not show when they are delayed and if the underlying element is gone', function () {
            expect(visibleTipsies().length).to.equal(0);

            helpers.hover($el);

            $el.remove();

            clock.tick(TIPSY_DELAY);
            expect(visibleTipsies().length).to.equal(0);
            expect($('.tipsy').length).to.not.be.defined;
        });

        it('trigger has aria-describedby should be on the link with correct id', function () {
            helpers.hover($el);
            clock.tick(TIPSY_DELAY);
            const tipsyId = visibleTipsies()[0].id;
            expect($el.attr('aria-describedby')).to.equal(tipsyId);
        });

        it('tooltip should have an id', function () {
            helpers.hover($el);
            clock.tick(TIPSY_DELAY);
            const tipsyId = visibleTipsies()[0].id;
            expect(visibleTipsies()[0].id).to.not.equal('');
        });
    }

    describe('on a <div>', function () {
        beforeEach(function () {
            $el = $('<div title="my element">Element</div>');
            $(document.body).append($el);
            $el.tooltip({
                delayIn: TIPSY_DELAY
            });
        });

        afterEach(function () {
            clock.restore();
            $el.remove();
            $(document.body).find('.tipsy').remove();
        });

        runSharedTooltipTests();

        it('tooltips can be destroyed with the "destroy" option', function () {
            $el.tooltip('destroy');
            clock.tick(TIPSY_DELAY);
            expect(visibleTipsies().length).to.equal(0);
            expect($('.tipsy').length).to.not.be.defined;
        });
    });

    describe('using live option', function () {
        const tooltipLiveDefault = $.fn.tooltip.defaults.live;

        beforeEach(function () {
            // Set tooltips to use live tooltips by default, which lets us destroy live tooltips with the "destroy" option.
            $.fn.tooltip.defaults.live = true;
        });

        afterEach(function () {
            clock.restore();
            $el.remove();
            $(document.body).find('.tipsy').remove();

            // Restore the default tooltip options.
            $.fn.tooltip.defaults.live = tooltipLiveDefault;
        });

        function runLiveTooltipDestroyTests() {
            it('live tooltips can be destroyed with the "destroy" option', function () {
                // Note: Destroying live tooltips with the "destroy" option only works when the default options are overridden.
                $('.has-tooltip').tooltip('destroy');

                helpers.hover($el);

                clock.tick(TIPSY_DELAY);
                expect(visibleTipsies().length).to.equal(0);
                expect($('.tipsy').length).to.not.be.defined;
            });
        }

        function runLiveTooltipTests () {
            runSharedTooltipTests();
            runLiveTooltipDestroyTests();

            describe('on a dynamically added element', function () {
                beforeEach(function () {
                    $el = $('<div class="has-tooltip" title="my element-2">Element 2</div>');
                    $(document.body).append($el);
                });

                runSharedTooltipTests();
                runLiveTooltipDestroyTests();
            });
        }

        describe('on an empty collection', function () {
            beforeEach(function () {
                $('.has-tooltip').tooltip({
                    delayIn: TIPSY_DELAY
                });

                $el = $('<div class="has-tooltip" title="my element">Element</div>');
                $(document.body).append($el);
            });

            runLiveTooltipTests();
        });

        describe('on a non-empty collection', function () {
            beforeEach(function () {
                $el = $('<div class="has-tooltip" title="my element">Element</div>');
                $(document.body).append($el);

                $('.has-tooltip').tooltip({
                    delayIn: TIPSY_DELAY
                });
            });

            runLiveTooltipTests();
        });
    });

    describe('on svgs', function () {
        const TIPSY_MARGIN = 50;
        let $rect;

        beforeEach(function () {
            clock = sinon.useFakeTimers();
            const svgHtml = '<svg width="300" height="300">' +
                '<rect id="svg-rect" x="50" y="50" width="50" height="50" fill="red" />' +
                '</svg>';
            $el = $(svgHtml);
            $(document.getElementById('test-fixture')).append($el);
            $rect = $('rect');

            $rect.tooltip({
                delayIn: 0,
                title: function () { return 'an svg element'; }
            });
        });

        afterEach(function () {
            clock.restore();
            $el.remove();
            $(document.body).find('.tipsy').remove();
        });

        it('will position correctly when attached to an svg element', function () {
            helpers.hover($rect);
            clock.tick(1);

            const $tipsies = visibleTipsies();
            const tipsyPosition = $tipsies.position();
            const rectPosition = $rect.position();
            const rectSize = $rect[0].getBoundingClientRect().width

            expect($tipsies.length).to.equal(1);
            expect(tipsyPosition.left).to.be.within(rectPosition.left - TIPSY_MARGIN, rectPosition.left + rectSize + TIPSY_MARGIN);
            expect(tipsyPosition.top).to.be.within(rectPosition.top + rectSize, rectPosition.top + rectSize + TIPSY_MARGIN);
        });
    });
});
