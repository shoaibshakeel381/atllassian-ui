'use strict';

var gulp = require('gulp');
var eslint = require('gulp-eslint');
var jscs = require('gulp-jscs');
var minimist = require('minimist');
var stylish = require('gulp-jscs-stylish');

module.exports = function () {
    var options = minimist(process.argv.slice(2), {
        string: 'files',
        default: {
            files: ''
        }
    });

    return gulp.src(options.files.split(','))
        .pipe(eslint())
        .pipe(eslint.format())
        .pipe(jscs())
        .on('error', function () { /* noop */ })
        .pipe(stylish())
        .pipe(eslint.failAfterError());
};
