---
component: Inline Dialog
layout: main-layout.html
analytics:
  pageCategory: component
  component: inline-dialog
  componentApiType: web-component
---

<a href="https://design.atlassian.com/latest/product/components/inline-dialog/" class="aui-button aui-button-link docs-meta-link">Design guidelines</a>
<div class="aui-message aui-message-info">
    This documentation is for the <strong>web component Inline Dialog API</strong>.
    <ul class="aui-nav-actions-list"><li><a href="inline-dialog-deprecated.html">View documentation for the deprecated javascript-based Inline Dialog 2 API.</a></li></ul>
</div>


<p>
    A container for secondary content/controls to be displayed on user request. Consider this
    component as displayed in context to the triggering control with the dialog overlaying the page
    content.
</p>
<p>
    An inline dialog should be preferred over a modal dialog when a connection between the action
    has a clear benefit versus having a lower user focus.
</p>

<h3>Status</h3>
<table class="aui summary">
    <tbody>
    <tr>
        <th>API status:</th>
        <td><aui-lozenge type="success">general</aui-lozenge></td>
    </tr>
    <tr>
        <th>Included in AUI core?</th>
        <td><aui-lozenge type="current">Not in core</aui-lozenge> You must explicitly require the web
            resource key.
        </td>
    </tr>
    <tr>
        <th>Web resource key:</th>
        <td class="resource-key" data-resource-key="com.atlassian.auiplugin:aui-inline-dialog2"><code>com.atlassian.auiplugin:aui-inline-dialog2</code>
        </td>
    </tr>
    <tr>
        <th>AMD Module key:</th>
        <td class="resource-key">N/A</td>
    </tr>
    <tr>
        <th>Experimental since:</th>
        <td>5.7</td>
    </tr>
    <tr>
        <th>General API status:</th>
        <td>5.9</td>
    </tr>
    </tbody>
</table>


<h3>Example</h3>

<p>
    Associate a trigger (<code class="first-use">data-aui-trigger</code>) to an
    inline dialog by setting the trigger's <code class="first-use">aria-controls</code>
    attribute to the <code class="first-use">id</code> of the inline dialog:
</p>

<aui-docs-example live-demo>
    <noscript is="aui-docs-code" type="text/html">
        <a data-aui-trigger aria-controls="more-details" href="#more-details">
            Inline dialog trigger
        </a>
        <aui-inline-dialog id="more-details">
            <p>Lorem ipsum.</p>
        </aui-inline-dialog>
    </noscript>
</aui-docs-example>

<div class="aui-message aui-message-info">
    <h4>Buttons as triggers</h4>
    <p>
        <a href="buttons.html">Buttons</a> can serve as triggers as long as they
        have the <code>data-aui-trigger</code> boolean attribute.
    </p>
</div>

<h3>Behaviour</h3>

<h4>Opening</h4>

<p>
    An inline dialog will, by default, open when a user clicks its trigger, but
    can be made to open
</p>
<ul>
    <li><a href="#programmatically-opening">programmatically</a>,</li>
    <li><a href="#opening-at-page-load">automatically at page load</a>, or</li>
    <li><a href="#opening-by-hovering">when hovering over its trigger</a>.</li>
</ul>

<h5 id="programmatically-opening">Programmatically opening</h5>
<p>
    To programmatically open an inline dialog, set its
    <code class="first-use">open</code> property or add the <code>open</code>
    boolean attribute:
</p>

<noscript is="aui-docs-code" type="text/js">
    var inlineDialog = document.getElementById('my-inline-dialog');
    inlineDialog.open = true;
    inlineDialog.setAttribute('open', '');  // Equivalent to line above.
</noscript>

<h5 id="opening-at-page-load">Opening at page load</h5>
<p>
    To make an inline dialog open at page load, simply specify the
    <code>open</code> boolean attribute:
</p>

<aui-docs-example live-demo>
    <noscript is="aui-docs-code" type="text/html">
        <a data-aui-trigger aria-controls="start-open" href="#start-open">
            This inline dialog starts open
        </a>
        <aui-inline-dialog id="start-open" open>
            <p>Lorem ipsum.</p>
            <a class="aui-button aui-button-link">Close</a>
        </aui-inline-dialog>
    </noscript>
    <noscript type="text/js">
        AJS.$(function () {
            var inlineDialog = document.getElementById('start-open');
            inlineDialog.persistent = true;
            inlineDialog.querySelector('.aui-button').addEventListener('click', function (e) {
                inlineDialog.open = false;
            });
        });
    </noscript>
</aui-docs-example>

<div class="aui-message aui-message-warning">
    <h5>Boolean attributes</h5>
    <p>
        As per the <a href="https://html.spec.whatwg.org/multipage/infrastructure.html#boolean-attributes">
        boolean attribute spec</a>, a boolean attribute such as
        <code>open="false"</code> is interpreted as <em>true</em>.
    </p>
</div>

<h5 id="opening-by-hovering">Opening by hovering over the trigger</h5>
<p>
    Make an inline dialog show when hovering over the trigger by setting
    the inline dialog's <code class="first-use">responds-to</code> attribute:
</p>

<aui-docs-example live-demo>
    <noscript is="aui-docs-code" type="text/html">
        <a data-aui-trigger aria-controls="show-on-hover" href="#show-on-hover">
            Hover to show contents
        </a>
        <aui-inline-dialog id="show-on-hover" responds-to="hover">
            <p>Lorem ipsum.</p>
        </aui-inline-dialog>
    </noscript>
</aui-docs-example>

<p>
    Or by setting the <code class="first-use">respondsTo</code> JavaScript
    property:
</p>

<noscript is="aui-docs-code" type="text/js">
    var inlineDialog = document.getElementById('show-on-hover')
    inlineDialog.respondsTo = 'hover';
</noscript>

<div class="aui-message aui-message-hint">
    <h5>Hovering includes having keyboard focus</h5>
    <p>
        As an accessibility enchancement, giving an inline dialog's trigger
        focus will also enable hovering behaviour.
    </p>
</div>


<h4>Closing</h4>

<p>
    Inline dialogs will automatically close when the user clicks outside the
    inline dialog or presses ESC. Inline dialogs that
    <a href="#opening-by-hovering">open on hover</a> close when the user stops
    hovering over either the trigger or the inline dialog itself.
</p>

<p>
    An inline dialog that is set to <a href="#prevent-automatic-closing">not
    automatically close</a> can only be
    <a href="#programmatically-closing">closed programmatically</a>, e.g., via an
    event handler bound to a close button.
</p>

<h5 id="programmatically-closing">Programmatically closing</h5>

<p>
    To programmatically close an inline dialog, set its <code>open</code>
    property or remove its <code>open</code> attribute:
</p>

<noscript is="aui-docs-code" type="text/js">
    var inlineDialog = document.getElementById('my-inline-dialog');
    inlineDialog.open = false;
    inlineDialog.removeAttribute('open');  // Equivalent to line above.
</noscript>

<h5 id="prevent-automatic-closing">Prevent automatic closing</h5>

<p>
    Inline dialogs can be forced to remain open via the
    <code class="first-use">persistent</code> boolean attribute:
</p>

<aui-docs-example live-demo>
    <noscript is="aui-docs-code" type="text/html">
        <a data-aui-trigger aria-controls="stays-open" href="#stays-open">
            This inline dialog stays open
        </a>
        <aui-inline-dialog id="stays-open" persistent>
            <p>Lorem ipsum.</p>
            <a class="aui-button aui-button-link">Close</a>
        </aui-inline-dialog>
    </noscript>
    <noscript type="text/js">
        AJS.$(function () {
            var inlineDialog = document.getElementById('stays-open');
            inlineDialog.querySelector('.aui-button').addEventListener('click', function (e) {
                inlineDialog.open = false;
            });
        });
    </noscript>
</aui-docs-example>

<p>
    Or by setting the <code>persistent</code> JavaScript property:
</p>

<noscript is="aui-docs-code" type="text/js">
    var inlineDialog = document.getElementById('stays-open')
    inlineDialog.persistent = true;
</noscript>


<h3>Apperance</h3>

<h4>Alignment</h4>

<p>An inline dialog can be aligned, relative to its trigger, along twelve different points, via the
    <code class="first-use">alignment</code> attribute:
</p>

<aui-docs-example live-demo>
    <noscript is="aui-docs-code" type="text/html">
        <a data-aui-trigger aria-controls="align-bottom-right" href="#align-bottom-right">
            Bottom right alignment
        </a>
        <aui-inline-dialog id="align-bottom-right" alignment="bottom right">
            <p>Lorem ipsum.</p>
        </aui-inline-dialog>
    </noscript>
</aui-docs-example>

<p>
    The <code>alignment</code> attribute takes two positional arguments in the
    format <code>alignment="<em>edge</em> <em>edge-position</em>"</code>, where
</p>

<ul>
    <li>
        <code><em>edge</em></code> specifies what edge to align the inline
        dialog's arrow to, and
    </li>
    <li>
        <code><em>edge-position</em></code> specifies where on that edge the
        arrow should appear.
    </li>
</ul>

<p>
    See the <a href="#api-reference-alignment"><code>alignment</code> API
    reference</a> for all valid combinations, or test the combinations below:
</p>


<aui-docs-example live-demo>
    <noscript type="text/html">
        <div class="aui-buttons">
            <button id="alignment-combinations-demo-trigger"
                    class="aui-button aui-button-split-main"
                    data-aui-trigger aria-controls="alignment-combinations-demo"
                ><code>alignment="<span class="edge">top</span> <span class="edge-alignment">left</span>"</code>
            </button>
            <button class="aui-button aui-dropdown2-trigger aui-button-split-more"
                    id="alignment-combinations-dropdown-trigger"
                    data-aui-trigger aria-controls="alignment-combinations-dropdown"
                >Alignment</button>

            <div id="alignment-combinations-demo-container"></div>
            <aui-dropdown-menu id="alignment-combinations-dropdown">
                <aui-section label="Edge">
                    <aui-item-radio data-edge-type="horizontal" interactive checked><code>top</code></aui-item-radio>
                    <aui-item-radio data-edge-type="vertical" interactive><code>right</code></aui-item-radio>
                    <aui-item-radio data-edge-type="horizontal" interactive><code>bottom</code></aui-item-radio>
                    <aui-item-radio data-edge-type="vertical" interactive><code>left</code></aui-item-radio>
                </aui-section>
                <aui-section data-edge-type="horizontal" label="Horizontal edge alignment">
                    <aui-item-radio interactive checked><code>left</code></aui-item-radio>
                    <aui-item-radio interactive><code>center</code></aui-item-radio>
                    <aui-item-radio interactive><code>right</code></aui-item-radio>
                </aui-section>
                <aui-section data-edge-type="vertical" label="Vertical edge alignment">
                    <aui-item-radio interactive disabled checked><code>top</code></aui-item-radio>
                    <aui-item-radio interactive disabled><code>middle</code></aui-item-radio>
                    <aui-item-radio interactive disabled><code>bottom</code></aui-item-radio>
                </aui-section>
            </aui-dropdown-menu>
        </div>
    </noscript>
    <noscript type="text/css">
        #alignment-combinations-demo-trigger {
            width: 250px;
        }
    </noscript>
    <noscript type="text/js">
        AJS.$(function ($) {

            var $alignmentChoicesTrigger = $('#alignment-combinations-dropdown-trigger');
            var $alignmentChoices = $('#' + $alignmentChoicesTrigger.attr('aria-controls'));

            var $demoTrigger = $('#alignment-combinations-demo-trigger');
            var $demoContainer = $('#alignment-combinations-demo-container');

            var oppositeEdge = {
                horizontal: 'vertical',
                vertical: 'horizontal'
            };

            function enableEdgeAlignmentChoices(edgeType) {
                var $radios = $alignmentChoices
                    .find('aui-section[data-edge-type="' + edgeType + '"] aui-item-radio');
                $radios.removeProp('disabled');

                $radios = $alignmentChoices
                    .find('aui-section[data-edge-type="' + oppositeEdge[edgeType] + '"] aui-item-radio');
                $radios.prop('disabled', '');
            }

            function updateInlineDialogPosition() {
                var $selectedEdge = $alignmentChoices
                    .find('aui-section aui-item-radio[data-edge-type][checked]');
                var edgeType = $selectedEdge.attr('data-edge-type');
                var $selectedEdgeAlignment = $alignmentChoices
                    .find('aui-section[data-edge-type="' + edgeType + '"] aui-item-radio[checked]');

                var edge = $selectedEdge.text();
                var edgeAlignment = $selectedEdgeAlignment.text();

                $demoTrigger.find('.edge').text(edge);
                $demoTrigger.find('.edge-alignment').text(edgeAlignment);

                var dropdownId = $demoTrigger.attr('aria-controls');
                var alignment = edge + ' ' + edgeAlignment;
                $('#' + dropdownId).remove();
                $demoContainer.html(
                    '<aui-inline-dialog id="' + dropdownId + '" alignment="' + alignment + '">Lorem ipsum.</aui-inline-dialog>'
                );
            }

            $alignmentChoices.on(
                'aui-dropdown2-item-check', 'aui-item-radio[data-edge-type]',
                function () {
                    enableEdgeAlignmentChoices($(this).attr('data-edge-type'));
                    updateInlineDialogPosition();
                }
            );

            $alignmentChoices.on(
                'aui-dropdown2-item-check', 'aui-section[data-edge-type] aui-item-radio',
                updateInlineDialogPosition
            );

            updateInlineDialogPosition();
        });
    </noscript>
</aui-docs-example>

<div class="aui-message aui-message-warning">
    <h5>Alignment is rendered once</h5>
    <p>
        An inline dialog's <code>alignment</code> cannot be changed once the
        inline dialog has been opened. However, it will continue to be
        <a href="#space-constrained">responsive to space constraints</a>.
    </p>
</div>

<h5 id="space-constrained">Space-constrained alignment</h5>

<p>
    If there is not enough room to display an inline dialog with the desired
    alignment the inline dialog will flip alignment.
</p>

<aui-docs-example live-demo>
    <noscript is="aui-docs-code" type="text/html">
        <a data-aui-trigger aria-controls="try-right-middle" href="#try-right-middle">
            Right middle alignment, but flips left
        </a>
        <aui-inline-dialog id="try-right-middle" alignment="right middle">
            <p>Lorem ipsum.</p>
        </aui-inline-dialog>
    </noscript>
    <noscript type="text/css">
        a[aria-controls="try-right-middle"] {
            float: right;
            text-align: right;
        }
    </noscript>
</aui-docs-example>


<h3>API Reference</h3>

<h4>Attributes and properties</h4>
<table class="aui" id="dialog-methods">
    <thead>
        <tr>
            <th>Name</th>
            <th>Attribute</th>
            <th>Property</th>
            <th>Type</th>
            <th class="description">Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><code>id</code></td>
            <td><span class="aui-icon aui-icon-small aui-iconfont-success">is an attribute</span></td>
            <td><span class="aui-icon aui-icon-small aui-iconfont-success">is a property</span></td>
            <td>String</td>
            <td>
                <p>Required when using a trigger to interact with an inline dialog but not required for the inline dialog to function.</p>
                <p>Defaults to <code>null</code>.</p>
            </td>
        </tr>
        <tr>
            <td><code id="api-reference-alignment">alignment</code></td>
            <td><span class="aui-icon aui-icon-small aui-iconfont-success">is an attribute</span></td>
            <td><span class="aui-icon aui-icon-small aui-iconfont-close-dialog">is not a property</span></td>
            <td>String</td>
            <td>
                <p>Specifies an inline dialog's alignment with respect to its trigger. The inline dialog is not positioned if this is not specified.</p>
                <p>Defaults to <code>"right middle"</code>.</p>
                <div id="alignment-values-table" class="aui-expander-content" aria-hidden="true">
                    <table class="aui">
                        <tr class="top-row">
                            <td></td>
                            <td><code>top left</code></td>
                            <td><code>top center</code></td>
                            <td><code>top right</code></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><code>left top</code></td>
                            <td colspan="3" rowspan="3" class="trigger-cell">
                                Inline dialog trigger
                            </td>
                            <td><code>right top</code></td>
                        </tr>
                        <tr>
                            <td><code>left middle</code></td>
                            <td><code>right middle</code> <span class="aui-lozenge">default</span></td>
                        </tr>
                        <tr>
                            <td><code>left bottom</code></td>
                            <td><code>right bottom</code></td>
                        </tr>
                        <tr class="bottom-row">
                            <td></td>
                            <td><code>bottom left</code></td>
                            <td><code>bottom center</code></td>
                            <td><code>bottom right</code></td>
                            <td></td>
                        </tr>
                    </table>
                </div>
                <p>
                    <a data-replace-text="Hide values" class="aui-expander-trigger" aria-controls="alignment-values-table">Show all values</a>
                </p>
            </td>
        </tr>
        <tr>
            <td><code id="api-reference-open">open</code></td>
            <td><span class="aui-icon aui-icon-small aui-iconfont-success">is an attribute</span></td>
            <td><span class="aui-icon aui-icon-small aui-iconfont-success">is a property</span></td>
            <td>Boolean</td>
            <td>
                <p>When set it either hides or shows the element based on whether the incoming value is falsy or truthy. When accessed it will return whether or not the inline dialog is open.</p>
                <p>Defaults to <code>false</code>.</p>
            </td>
        </tr>
        <tr>
            <td><code id="api-reference-persistent">persistent</code></td>
            <td><span class="aui-icon aui-icon-small aui-iconfont-success">is an attribute</span></td>
            <td><span class="aui-icon aui-icon-small aui-iconfont-success">is a property</span></td>
            <td>Boolean</td>
            <td>
                <p>Specifies that an inline dialog is persistent. Persistent inline dialogs cannot be closed by outside click or escape.</p>
                <p>Defaults to <code>false</code>.</p>
            </td>
        </tr>
        <tr>
            <td><code  id="api-reference-responds-to" style="white-space: nowrap">responds-to</code></td>
            <td><span class="aui-icon aui-icon-small aui-iconfont-success">is an attribute</span></td>
            <td><span class="aui-icon aui-icon-small aui-iconfont-success">is a property</span></td>
            <td>String</td>
            <td>
                <p>Determines the type of interaction a trigger will have with its inline dialog.</p>
                <p>
                    Values:
                </p>
                <ul>
                    <li><code>toggle</code> <aui-lozenge>default</aui-lozenge> - will respond to click event on the trigger.</li>
                    <li><code>hover</code> - will respond to mouseover, mouseout, focus, blur events on the trigger.</li>
                </ul>
            </td>
        </tr>
    </tbody>
</table>


<h4>Methods</h4>
<p>There are no methods.</p>


<h4>Events</h4>
<p>Events are triggered when inline dialogs are shown and hidden. These events are triggered natively on the component. You can bind to the the inline dialog element for instance specific events, or rely on event bubbling and bind to the document to receive events for every show and hide.</p>
<table class="aui" id="inline-dialog-events">
    <thead>
        <tr>
            <th>Event</th>
            <th class="description">Description</th>
            <th>Preventable</th>
            <th>Bubbles</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td id="api-reference-aui-show">aui-show</td>
            <td>Triggered before an inline dialog instance is shown.</td>
            <td><strong>Yes</strong>. Prevents it from showing.</td>
            <td>Yes</td>
        </tr>
        <tr>
            <td id="api-reference-aui-hide">aui-hide</td>
            <td>Triggered before an inline dialog instance is hidden.</td>
            <td><strong>Yes</strong>. Prevents it from hiding.</td>
            <td>Yes</td>
        </tr>
    </tbody>
</table>
