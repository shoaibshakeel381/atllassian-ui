'use strict';

var gulp = require('gulp');
var umd = require('../../lib/umd');

module.exports = function () {
    return gulp.src(umd.sources)
        .pipe(umd.pipeline())
        .pipe(gulp.dest('./lib'));
};
