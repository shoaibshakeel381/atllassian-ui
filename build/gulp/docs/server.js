'use strict';

var commander = require('commander');
var gulp = require('gulp');
var gulpWebserver = require('gulp-webserver');

commander
    .option('-h, --host [0.0.0.0]', 'The host to listen on.')
    .option('-p, --port [8000]', 'The port to listen on.');

var defaultHost = '0.0.0.0';
var defaultPort = 8000;

function getHost () {
    return commander.host || defaultHost;
}

function getPort () {
    return commander.port || defaultPort;
}

module.exports = function () {
    return gulp.src('.tmp')
        .pipe(gulpWebserver({
            host: getHost(),
            livereload: true,
            open: './docs/dist',
            port: getPort()
        }));
};

module.exports.getHost = getHost;
module.exports.getPort = getPort;
