import element from '../../../src/js/aui/element';

describe('aui/element', function () {
    it('should be an object', function () {
        expect(element).to.be.an('object');
    });

    it('should be exposed on the AJS namespace', function () {
        expect(AJS).to.contain({
            element: element
        });
    });
});
