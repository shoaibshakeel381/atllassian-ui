'use strict';

var glob = require('glob');
var path = require('path');

var buildPath = 'build/gulp';

module.exports = function () {
    console.log('Available Tasks');
    console.log('---------------');
    console.log();
    console.log('To see specific information about a task call the task with `--help`.');

    glob.sync(path.join(buildPath, '{*,**/*}.js')).forEach(function (file) {
        var name = file.replace(buildPath + '/', '').replace('.js', '');
        console.log('  ' + name);
    });
};
