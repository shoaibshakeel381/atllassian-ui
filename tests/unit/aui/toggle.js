'use strict';

import '../../../src/js/aui/toggle';
import $ from '../../../src/js/aui/jquery';
import helpers from '../../helpers/all';
import i18n from '../../../src/js/aui/i18n';
import skate from 'skatejs';
import { INPUT_SUFFIX } from '../../../src/js/aui/internal/constants';

describe('aui/toggle', function () {
    var ENABLED = 'Enabled';
    var DISABLED = 'Disabled';

    var toggle;
    var input;
    var clock;

    describe('Basic construction and initialization', function () {
        beforeEach(function () {
            var dom = helpers.fixtures({
                toggle: '<aui-toggle label="Foo"></aui-toggle>'
            });
            skate.init(dom.toggle);

            toggle = dom.toggle;
            input = dom.toggle._input;
        });

        it('creates a input element', function () {
            expect(input).to.exist;
            expect(input.type).to.equal('checkbox');
        });

        it('tooltip-on and tooltip-off are initialized', function () {
            expect(toggle.tooltipOn).to.equal(i18n.getText('aui.toggle.on'));
            expect(toggle.tooltipOff).to.equal(i18n.getText('aui.toggle.off'));
        });
    });

    describe('Attribute & properties - ', function () {
        var form;
        var newForm;

        beforeEach(function () {
            var dom = helpers.fixtures({
                toggle: `<aui-toggle id="my-toggle" checked disabled
                            form="my-form" name="toggle-name" value="toggle-value"
                            tooltip-on="${ENABLED}" tooltip-off="${DISABLED}" label="My label"></aui-toggle>`,
                form: '<form id="my-form"></form>',
                newForm: '<form id="new-form"></form>'
            });
            skate.init(dom.toggle);

            toggle = dom.toggle;
            input = dom.toggle._input;
            form = dom.form;
            newForm = dom.newForm;
        });

        describe('Basic behaviors -', function () {
            it('Attributes are copied over input', function () {
                expect(input.id).to.equal('my-toggle' + INPUT_SUFFIX);
                expect(input.checked).to.be.true;
                expect(input.disabled).to.be.true;
                expect(input.getAttribute('form')).to.equal('my-form');
                expect(input.name).to.equal('toggle-name');
                expect(input.value).to.equal('toggle-value');
                expect(input.getAttribute('aria-label')).to.equal('My label');
            });

            it('All attributes are accessible via properties', function () {
                expect(toggle.id).to.equal('my-toggle');
                expect(toggle.checked).to.be.true;
                expect(toggle.disabled).to.be.true;
                expect(toggle.form).to.equal(form);
                expect(toggle.name).to.equal('toggle-name');
                expect(toggle.value).to.equal('toggle-value');
                expect(toggle.busy).to.be.false;
                expect(toggle.tooltipOn).to.equal(ENABLED);
                expect(toggle.tooltipOff).to.equal(DISABLED);
                expect(toggle.label).to.equal('My label');
            });

            describe('Remove toggle attributes', function () {
                beforeEach(function (done) {
                    toggle.removeAttribute('id');
                    toggle.removeAttribute('checked');
                    toggle.removeAttribute('disabled');
                    toggle.removeAttribute('form');
                    toggle.removeAttribute('name');
                    toggle.removeAttribute('value');
                    toggle.removeAttribute('tooltip-on');
                    toggle.removeAttribute('tooltip-off');
                    toggle.removeAttribute('label');
                    helpers.afterMutations(done);
                });

                it('remove attributes on the input (or set to default)', function () {
                    expect(input.id).to.be.empty;
                    expect(input.checked).to.be.false;
                    expect(input.disabled).to.be.false;
                    expect(input.form).to.be.null;
                    expect(input.name).to.equal('');
                    expect(input.value).to.equal('on');
                    expect(input.hasAttribute('tooltip-on')).to.be.false;
                    expect(input.hasAttribute('tooltip-off')).to.be.false;
                    expect(input.hasAttribute('aria-label')).to.be.false;
                });

                it('remove property values (or set to default)', function () {
                    expect(toggle.checked).to.be.false;
                    expect(toggle.disabled).to.be.false;
                    expect(toggle.form).to.be.null;
                    expect(toggle.name).to.equal('');
                    expect(toggle.value).to.equal('on');
                    expect(toggle.tooltipOn).to.be.null;
                    expect(toggle.tooltipOff).to.be.null;
                    expect(toggle.label).to.be.null;
                });
            });

            describe('Change toggle properties -', function () {
                beforeEach(function (done) {
                    toggle.id = 'new-id';
                    toggle.checked = false;
                    toggle.disabled = false;
                    toggle.name = 'new-name';
                    toggle.value = 'new-value';
                    toggle.busy = true;
                    toggle.tooltipOn = 'new-tooltip-on';
                    toggle.tooltipOff = 'new-tooltip-off';
                    toggle.label = 'new-label';
                    helpers.afterMutations(done);
                });

                it('make sure property values are actually changed', function () {
                    expect(toggle.id).to.equal('new-id');
                    expect(toggle.checked).to.be.false;
                    expect(toggle.disabled).to.be.false;
                    expect(toggle.name).to.equal('new-name');
                    expect(toggle.value).to.equal('new-value');
                    expect(toggle.busy).to.be.true;
                    expect(toggle.tooltipOn).to.equal('new-tooltip-on');
                    expect(toggle.tooltipOff).to.equal('new-tooltip-off');
                    expect(toggle.label).to.equal('new-label');
                });

                it('change toggle attributes', function () {
                    expect(toggle.getAttribute('id')).to.equal('new-id');
                    expect(toggle.hasAttribute('checked')).to.be.false;
                    expect(toggle.hasAttribute('disabled')).to.be.false;
                    expect(toggle.getAttribute('name')).to.equal('new-name');
                    expect(toggle.getAttribute('value')).to.equal('new-value');
                    expect(toggle.getAttribute('tooltip-on')).to.equal('new-tooltip-on');
                    expect(toggle.getAttribute('tooltip-off')).to.equal('new-tooltip-off');
                    expect(toggle.getAttribute('label')).to.equal('new-label');
                });

                it('change input properties or attributes', function () {
                    expect(input.id).to.equal('new-id' + INPUT_SUFFIX);
                    expect(input.checked).to.equal(false, 'checked');
                    expect(input.disabled).to.equal(false, 'disabled');
                    expect(input.name).to.equal('new-name');
                    expect(input.value).to.equal('new-value');
                    expect(input.getAttribute('aria-busy')).to.equal('true');
                    expect(input.getAttribute('aria-label')).to.equal('new-label');
                });
            });
        });

        describe('checked -', function () {
            it('Checked state still in sync after the toggle button is clicked', function (done) {
                toggle.disabled = false;
                helpers.afterMutations(function () {
                    expect(toggle.checked).to.be.true;
                    toggle.click();
                    expect(toggle.checked).to.be.false;
                    toggle.checked = true;
                    helpers.afterMutations(function () {
                        expect(toggle.checked).to.be.true;
                        done();
                    });
                });
            });

            it('Checked state stays in sync after setting checked property and then clicking', function (done) {
                toggle.disabled = false;
                toggle.checked = false;

                helpers.afterMutations(function () {
                    toggle.click();

                    helpers.afterMutations(function () {
                        expect(toggle.checked).to.be.true;
                        expect(toggle.hasAttribute('checked')).to.be.true;
                        done();
                    });
                });
            });

            it('Checked state stays in sync after setting checked property to false and then setting the attribute directly', function (done) {
                toggle.disabled = false;
                toggle.checked = false;

                helpers.afterMutations(function () {
                    toggle.setAttribute('checked', '');

                    helpers.afterMutations(function () {
                        expect(toggle.checked).to.be.true;
                        expect(toggle.hasAttribute('checked')).to.be.true;
                        done();
                    });
                });
            });

            it('Checked state stays in sync after setting checked property to true and then removing the attribute directly', function (done) {
                toggle.disabled = false;
                toggle.checked = false;
                toggle.checked = true;

                helpers.afterMutations(function () {
                    toggle.removeAttribute('checked');

                    helpers.afterMutations(function () {
                        expect(toggle.checked).to.be.false;
                        expect(toggle.hasAttribute('checked')).to.be.false;
                        done();
                    });
                });
            });
        });

        describe('value -', function () {
            it('Assign null to value will set the value to empty string', function (done) {
                toggle.value = null;
                helpers.afterMutations(function () {
                    expect(toggle.value).to.equal('');
                    done();
                });
            });
        });
    });

    describe('Tooltips -', function () {
        function getTooltip () {
            return document.querySelector('[role="tooltip"]');
        }

        function removeAllTooltips() {
            $('[role="tooltip"]').remove();
        }

        beforeEach(function () {
            clock = sinon.useFakeTimers();
        });

        afterEach(function () {
            clock.restore();
            removeAllTooltips()
        });

        describe('Initialization -', function () {
            beforeEach(function () {
                var dom = helpers.fixtures({
                    toggle: '<aui-toggle label="Foo"></aui-toggle>'
                });
                skate.init(dom.toggle);

                toggle = dom.toggle;
            });

            it('show default text if tooltip-off is not set', function () {
                helpers.hover(toggle);
                clock.tick(500);
                var tooltip = getTooltip();
                expect(tooltip.textContent).to.equal(i18n.getText('aui.toggle.off'));
            });

            it('show default text if tooltip-on is not set', function () {
                toggle.checked = true;
                helpers.hover(toggle);
                clock.tick(500);
                var tooltip = getTooltip();
                expect(tooltip.textContent).to.equal(i18n.getText('aui.toggle.on'));
            });
        });

        describe('Behaviors -', function () {
            beforeEach(function () {
                var dom = helpers.fixtures({
                    toggle: `<aui-toggle label="Foo" tooltip-on="${ENABLED}" tooltip-off="${DISABLED}"/>`
                });
                skate.init(dom.toggle);

                toggle = dom.toggle;
            });

            it(`toggle shows a tooltip with '${DISABLED}' text in off state`, function () {
                helpers.hover(toggle);
                clock.tick(500);
                var tooltip = getTooltip();
                expect(tooltip).to.exist;
                expect(tooltip.textContent).to.equal(DISABLED);
            });

            it(`toggle shows a tooltip with '${ENABLED}' text in off state`, function () {
                toggle.checked = true;
                helpers.hover(toggle);
                clock.tick(500);
                var tooltip = getTooltip();
                expect(tooltip).to.exist;
                expect(tooltip.textContent).to.equal(ENABLED);
            });

            it('shown on disabled button', function () {
                toggle.disabled = true;
                helpers.hover(toggle);
                clock.tick(500);
                var tooltip = getTooltip();
                expect(tooltip).to.exist;
                expect(tooltip.textContent).to.equal(DISABLED);
            });
        });
    });

    describe('Behaviors -', function () {

        beforeEach(function () {
            var dom = helpers.fixtures({
                toggle: '<aui-toggle id="my-toggle" label="Foo"></aui-toggle>'
            });
            skate.init(dom.toggle);

            toggle = dom.toggle;
            input = dom.toggle._input;
        });

        describe('Normal toggle -', function () {
            it('toggle is focusable and tabbable', function () {
                var $items = $('#test-fixture :aui-focusable');
                expect($items.length).to.equal(1);
                $items = $('#test-fixture :aui-tabbable');
                expect($items.length).to.equal(1);
            });

            it('click can toggle', function () {
                helpers.click(toggle);
                expect(toggle.checked).to.be.true;
            });

            it('focus and press space to toggle', function () {
                helpers.focus(toggle);
                expect(document.activeElement.id).to.be.equal('my-toggle' + INPUT_SUFFIX);
                // cannot simulate SPACE, so use click event on active element instead
                document.activeElement.click();
                expect(toggle.checked).to.be.true;
            });
        });

        describe('Disabled toggle', function () {
            beforeEach(function (done) {
                toggle.disabled = true;
                helpers.afterMutations(done);
            });

            it('is not focusable and tabbable', function () {
                var $items = $('#test-fixture :aui-focusable');
                expect($items.length).to.equal(0);
                $items = $('#test-fixture :aui-tabbable');
                expect($items.length).to.equal(0);
            });

            it('click will not toggle', function () {
                helpers.click(toggle);
                expect(toggle.checked).to.be.false;
            });

            it('javascript - .focus() is not possible', function () {
                helpers.focus(toggle);
                expect(document.activeElement.hasAttribute('id')).to.be.false;
            });
        });

        describe('busy state', function () {
            function getSpinner() {
                return document.querySelector('.spinner');
            }

            beforeEach(function () {
                toggle.busy = true;
            });

            it('spinner is shown', function () {
                expect(getSpinner()).to.exist;
            });

            it('sets indeterminate on the input', function () {
                expect(input.indeterminate).to.equal(true);
                toggle.busy = false;
                expect(input.indeterminate).to.equal(false);
            });

            it('adds .indeterminate-checked on the input', function () {
                input.checked = true;
                expect(input.matches('.indeterminate-checked')).to.equal(false, 'pre-busy');
                toggle.busy = true;
                expect(input.matches('.indeterminate-checked')).to.equal(true, 'busy');
                toggle.busy = false;
                expect(input.matches('.indeterminate-checked')).to.equal(false, 'post-busy');
            });

            it('sets aria-busy on the input', function () {
                expect(input.getAttribute('aria-busy')).to.equal('true');
                toggle.busy = false;
                var ariaBusyValues = [null, 'false'];
                expect(ariaBusyValues.indexOf(input.getAttribute('aria-busy'))).to.not.equal(-1);
            });

            it('is focusable and tabbable', function () {
                var $items = $('#test-fixture :aui-focusable');
                expect($items.length).to.equal(1);
                $items = $('#test-fixture :aui-tabbable');
                expect($items.length).to.equal(1);
            });

            it('is not clickable', function () {
                helpers.click(toggle);
                expect(toggle.checked).to.be.false;
            });
        });
    });
});
