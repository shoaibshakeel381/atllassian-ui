'use strict';

import '../../../src/js/aui';
import * as events from '../../../src/js/aui/event';

describe('aui/event', function () {
    it('global', function () {
        expect(AJS).to.contain(events);
    });

    it('Binding', function () {
        var spy = sinon.spy();

        events.bind('test1-event', spy);
        events.trigger('test1-event');
        spy.should.have.been.calledOnce;
    });

    it('Unbinding', function () {
        var spy = sinon.spy();

        events.bind('test2-event', spy);
        events.trigger('test2-event');
        spy.should.have.been.calledOnce;

        events.unbind('test2-event');
        events.trigger('test2-event');
        spy.should.have.been.calledOnce;
    });
});
