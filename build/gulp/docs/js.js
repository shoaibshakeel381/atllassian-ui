'use strict';

var commander = require('../../lib/commander');
var debug = require('../../lib/debug');
var expect = require('../../lib/expect');
var galvatron = require('../../lib/galvatron');
var gulp = require('gulp');
var mac = require('mac');
var minify = require('../../lib/minify');
var streamIf = require('../../lib/stream-if');

module.exports = mac.parallel(function () {
    var bundle = galvatron.bundle('docs/src/scripts/index.js');
    return gulp.src(bundle.files)
        .pipe(bundle.watchIf(commander.watch))
        .pipe(debug({title: 'docs/js/compile'}))
        .pipe(bundle.stream())
        .pipe(streamIf(!commander.watch, debug({title: 'docs/js/optimise'})))
        .pipe(streamIf(!commander.watch, minify))
        .pipe(gulp.dest('.tmp/docs/dist/scripts'))
        .pipe(expect(bundle.files));
});
