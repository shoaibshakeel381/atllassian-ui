'use strict';

import version from '../../../src/js/aui/version';

describe('aui/version', function () {
    it('globals', function () {
        expect(AJS).to.contain({
            version: version
        });
    });

    it('should be a string', function () {
        expect(version).to.be.a('string');
    });

    it('should have correct version format', function () {
        expect(version).to.match(/^\d+\.\d+\.\d+(-.*)?/);
    });
});
