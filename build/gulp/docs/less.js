'use strict';

var commander = require('../../lib/commander');
var debug = require('../../lib/debug');
var expect = require('../../lib/expect');
var gulp = require('gulp');
var gulpLess = require('gulp-less');
var gulpMinifyCss = require('gulp-minify-css');
var gulpWatchLess = require('gulp-watch-less');
var mac = require('mac');
var path = require('path');
var streamIf = require('../../lib/stream-if');

var src = 'docs/src/styles/index.less';
var targetFolder = '.tmp/docs/dist/styles';

module.exports = mac.parallel(
    function () {
        return gulp.src(src)
            .pipe(streamIf(commander.watch, gulpWatchLess.bind(null, src)))
            .pipe(debug({title: 'docs/less/compile'}))
            .pipe(gulpLess({
                paths: [
                    path.basename(src)
                ]
            }))
            .pipe(streamIf(!commander.watch, debug({title: 'docs/less/optimise'})))
            .pipe(streamIf(!commander.watch, gulpMinifyCss()))
            .pipe(gulp.dest(targetFolder))
            .pipe(expect(src));
    },
    function () {
        return gulp.src([
            'src/css-vendor/jquery/plugins/*.png',
            'src/css-vendor/jquery/plugins/*.gif'
        ]).pipe(gulp.dest(targetFolder));
    }
);
