'use strict';

import $ from '../jquery';
import skate from 'skatejs';

(function () {
    if ('placeholder' in document.createElement('input')) {
        return;
    }

    function applyDefaultText (input) {
        var value = String(input.value).trim();
        if (!value.length) {
            input.value = input.getAttribute('placeholder');
            $(input).addClass('aui-placeholder-shown');
        }
    }

    skate('placeholder', {
        type: skate.type.ATTRIBUTE,
        events: {
            blur: applyDefaultText,
            focus: function (input) {
                if (input.value === input.getAttribute('placeholder')) {
                    input.value = '';
                    $(input).removeClass('aui-placeholder-shown');
                }
            }
        },
        created: applyDefaultText
    });
}());
