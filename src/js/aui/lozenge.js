'use strict'

import globalize from './internal/globalize';
import skate from 'skatejs';

var lozenge = skate('aui-lozenge', {

    attributes: {
        display: {},
        type: {}
    }

});

export default globalize('lozenge', lozenge);
