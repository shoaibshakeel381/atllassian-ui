'use strict';

import template from '../../../src/js/aui/template';

describe('aui/template', function () {
    it('globals', function () {
        expect(AJS).to.contain({
            template: template
        });
    });

    it('handles html escaping correctly', function () {
        var tmp = template('Hello, {name}. Welcome to {application}.<br>');

        expect(tmp.fill({
                name: '"O\'Foo"',
                application: '<JIRA & Confluence>'
            }).toString())
            .to
            .equal('Hello, &quot;O&#39;Foo&quot;. Welcome to &lt;JIRA &amp; Confluence&gt;.<br>');
    });
});
