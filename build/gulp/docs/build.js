'use strict';

var auiDist = require('../dist');
var copyAndWatch = require('../../lib/copy-and-watch');
var docsClean = require('./clean');
var docsJs = require('./js');
var docsLess = require('./less');
var docsMetalsmith = require('./metalsmith');
var mac = require('mac');

module.exports = mac.series(
    docsClean,
    copyAndWatch('docs/**', '.tmp/docs'),
    docsMetalsmith,
    mac.parallel(
        auiDist,
        docsJs,
        docsLess
    )
);
