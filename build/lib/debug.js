'use strict';

var commander = require('./commander');
var gulpDebug = require('gulp-debug');
var streamIf = require('./stream-if');

module.exports = function (opts) {
    return streamIf(commander.debug, gulpDebug(opts));
};
