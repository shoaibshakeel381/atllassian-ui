'use strict';

var babel = require('babel-core');
var galvatron = require('galvatron');
var minimatch = require('minimatch');
var path = require('path');
var versionReplace = require('./version-replace');

var galv = galvatron();
var root = path.join(__dirname, '..', '..');

galv.fs.map({
    // This is required because Backbone imports "jquery" and that will import
    // the entire jQuery source if we don't tell it to use our shim instead.
    jquery: path.join(root, 'src', 'js', 'aui', 'jquery.js'),

    // Once we don't have modified sources we can remove this to resolve them
    // from our node_modules. For now, this fixes the issue when we require
    // Backbone it tries to require Underscore using its module name rather
    // than it's path. This would then normally look in the node_modules
    // directory, but the map forces it to resolve to a path.
    //
    // This also lets consumers of the `lib/` build use the versions of underscore
    // and backbone specified in their `package.json`.
    backbone: path.join(root, 'src', 'js-vendor', 'backbone', 'backbone.js'),
    underscore: path.join(root, 'src', 'js-vendor', 'underscorejs', 'underscore.js')
});

galv.transformer
    .post(function (code, data) {
        var isJsVendor = minimatch(data.path, '**/js-vendor/**/*');
        if (isJsVendor) {
            return code;
        }
        var transformedCode = babel.transform(code, {
            blacklist: ['useStrict']
        }).code;
        return transformedCode;
    })
    .post(versionReplace)
    .post('globalize');

module.exports = galv;
