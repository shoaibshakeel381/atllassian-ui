'use strict';

var fs = require('fs-extra');
var gulp = require('gulp');
var copyLibLess = require('../../lib/copy-lib-less');
var mac = require('mac');

function cleanup () {
    fs.removeSync('lib/css');
    fs.mkdirsSync('lib/css');
}

module.exports = mac.series(
    cleanup,
    copyLibLess()
);
