'use strict';

import globalize from './internal/globalize';

var version = '${project.version}';

globalize('version', version);

export default version;
