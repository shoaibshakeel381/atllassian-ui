---
component: Buttons
layout: main-layout.html
analytics:
  pageCategory: component
  component: button
---
<a href="https://design.atlassian.com/latest/product/components/buttons/" class="aui-button aui-button-link docs-meta-link">Design guidelines</a>

<h3>Summary</h3>

<p>Buttons are used as triggers for actions. They are used in forms, toolbars, dialog footers and as stand-alone action triggers. Buttons should rarely, if ever, be used for navigation.</p>
<p><strong>Actions</strong> are operations that are performed on objects.</p><p><strong>Navigation</strong> changes the screen or view or takes you to another context in the application.</p>

<h3>Status</h3>
<table class="aui summary">
    <tbody>
        <tr>
            <th>API status:</th>
            <td><aui-lozenge type="success">general</aui-lozenge></td>
        </tr>
        <tr>
            <th>Included in AUI core?</th>
            <td>Yes. You do not need to explicitly require the web resource key.</td>
        </tr>
        <tr>
            <th>Web resource key:</th>
            <td class="resource-key" data-resource-key="com.atlassian.auiplugin:aui-buttons"><code>com.atlassian.auiplugin:aui-buttons</code></td>
        </tr>
        <tr>
            <th>Experimental since:</th>
            <td>4.2</td>
        </tr>
        <tr>
            <th>General API status:</th>
            <td>5.1</td>
        </tr>
    </tbody>
</table>

<h3>Examples</h3>

<div class="aui-flatpack-example">
    <p>
        <button class="aui-button">Button</button>
    </p>

    <p>
        <button class="aui-button aui-button-primary">Primary Button </button>
    </p>

    <p>
        <button class="aui-button aui-button-link">Link button</button>
    </p>

    <p>
        <button class="aui-button aui-dropdown2-trigger" aria-owns="dropdown2-more" aria-haspopup="true">Dropdown button</button>
            </p><div id="dropdown2-more" class="aui-dropdown2 aui-style-default">
                <ul class="aui-list-truncate">
                    <li><a href="http://example.com/">Menu item 1</a></li>
                    <li><a href="http://example.com/">Menu item 2</a></li>
                    <li><a href="http://example.com/">Menu item 3</a></li>
                </ul>
            </div>
        <p></p>
        <p>
        <button class="aui-button"><span class="aui-icon aui-icon-small aui-iconfont-view">View </span> Icon button</button>
    </p>

    <p>
        <button class="aui-button" aria-disabled="true">Disabled button</button>
    </p>

    <p>
        <button class="aui-button aui-button-subtle"><span class="aui-icon aui-icon-small aui-iconfont-configure">Configure </span> Subtle button</button>
    </p>

    <p>
        <div id="split-button-demo" class="aui-buttons">
            <button class="aui-button aui-button-split-main">Split button</button>
            <button class="aui-button aui-dropdown2-trigger aui-button-split-more"
                    aria-owns="split-container-dropdown" aria-haspopup="true">Split button more</button>
        </div>
        <div id="split-container-dropdown" class="aui-dropdown2 aui-style-default" data-container="split-button-demo"
             role="menu" aria-hidden="true">
            <ul class="aui-list-truncate">
                <li><a href="http://example.com/">Menu item 1</a></li>
                <li><a href="http://example.com/">Menu item 2</a></li>
                <li><a href="http://example.com/">Menu item 3</a></li>
            </ul>
        </div>
    </p>

    <p class="aui-buttons">
        <button class="aui-button">Button</button>
        <button class="aui-button">Button</button>
        <button class="aui-button">Button</button>
    </p>

</div>

<h3>Code</h3>

<h4>HTML</h4>

<p>The base button code is:</p>
<aui-docs-example>
    <noscript is="aui-docs-code" type="text/html">
        <button class="aui-button">Button</button>
    </noscript>
</aui-docs-example>

<p>You can then apply a button type by adding the appropriate class, for example <code>aui-button-primary</code>:</p>

<aui-docs-example>
    <noscript is="aui-docs-code" type="text/html">
        <button class="aui-button aui-button-primary">Button</button>
    </noscript>
</aui-docs-example>

<p>Button types and classes:</p>

<ul>
    <li>Standard/default - (no extra class)</li>
    <li>Primary - <code>aui-button-primary</code></li>
    <li>Link-style (used for "cancel" actions) - <code>aui-button-cancel</code></li>
    <li>Subtle (looks like a link while inactive, looks like a button when hovered/focused) - <code>aui-button-subtle</code></li>
    <li>Split (has a primary and secondary area within one button)</li>
</ul>

<p>Button states are applied using boolean ARIA attributes:</p>
<aui-docs-example>
    <noscript is="aui-docs-code" type="text/html">
        <button class="aui-button" aria-disabled="true" disabled>Button</button>
        <button class="aui-button" aria-pressed="true">Button</button>
    </noscript>
</aui-docs-example>

<p>Button states:</p>
<ul>
    <li>Disabled (Buttons provides the disabled <em>style</em> but you still need to disable any events, etc) - <code>aria-disabled="true"</code>.</li>
    <li>Pressed (a pressed/enabled style for toggle buttons) - - <code>aria-pressed="true"</code></li>
</ul>

<p>Note the style will apply when the attribute is present and set to true.</p><p>Button groups are created by wrapping buttons in an <code>aui-buttons</code> (note plural) DIV element:</p>
<aui-docs-example>
    <noscript is="aui-docs-code" type="text/html">
        <div class="aui-buttons">
            <button class="aui-button">Button</button>
            <button class="aui-button">Button</button>
            <button class="aui-button">Button</button>
        </div>
    </noscript>
</aui-docs-example>

<p>Split buttons require a wrapper and extra modifier classes; the second button should be a Dropdown2 trigger:</p>
<aui-docs-example>
    <noscript is="aui-docs-code" type="text/html">
        <div class="aui-buttons">
            <button class="aui-button aui-button-split-main">Split main</button>
            <button class="aui-button aui-dropdown2-trigger aui-button-split-more" aria-haspopup="true" aria-owns="split-container-dropdown">Split more</button>
        </div>
    </noscript>
</aui-docs-example>
<p>Note you can use Dropdown2's <code>data-container</code> feature to force the dropdown to extend right to left, setting the container with an ID on the <code>aui-buttons</code> element.</p>

<h4>Soy</h4>

<p>Single buttons:</p>
<noscript is="aui-docs-code" type="text/handlebars">
    {call aui.buttons.button}
        {param text: 'Button'/}
    {/call}

    {call aui.buttons.button}
        {param text: 'Primary Button'/}
        {param type: 'primary'/}
    {/call}
</noscript>

<p>Dropdown2 button:</p>
<noscript is="aui-docs-code" type="text/handlebars">
    {call aui.buttons.button}
        {param text: 'Dropdown button'/}
        {param type: 'link'/}
        {param dropdown2Target: 'dropdown2id'/}
    {/call}
</noscript>
<p>Icon button:</p>
<noscript is="aui-docs-code" type="text/handlebars">
    {call aui.buttons.button}
        {param text: ' Icon Button' /}
        {param iconType: 'aui' /}
        {param iconClass: 'aui-icon-small aui-iconfont-view' /}
        {param iconText: 'View' /}
    {/call}
</noscript>
<p>Grouped buttons:</p>
<noscript is="aui-docs-code" type="text/handlebars">
    {call aui.buttons.buttons}
        {param content}
            {call aui.buttons.button}{param text: 'Button'/}{/call}
            {call aui.buttons.button}{param text: 'Button'/}{/call}
            {call aui.buttons.button}{param text: 'Button'/}{/call}
        {/param}
    {/call}
</noscript>

<p>Split buttons:</p>
<noscript is="aui-docs-code" type="text/handlebars">
    {call aui.buttons.buttons}
        {param content}
            {call aui.buttons.splitButton}
                {param splitButtonMain: [
                    'text': 'Split main'
                ] /}
                {param splitButtonMore: [
                    'text': 'Split more',
                    'dropdown2Target': 'split-container-dropdown'
                ] /}
            {/call}
        {/param}
    {/call}
</noscript>

<p>Disabled button:</p>

<noscript is="aui-docs-code" type="text/handlebars">
    {call aui.buttons.button}
        {param text: 'Button'/}
        {param isDisabled: 'true'/}
    {/call}
</noscript>

<p>Pressed button:</p>
<noscript is="aui-docs-code" type="text/handlebars">
    {call aui.buttons.button}
        {param text: 'Button'/}
        {param isPressed: 'true'/}
    {/call}
</noscript>
