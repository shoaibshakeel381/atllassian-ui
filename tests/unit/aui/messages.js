'use strict';

import $ from '../../../src/js/aui/jquery';
import helpers from '../../helpers/all';
import keyCode from '../../../src/js/aui/key-code';
import messages from '../../../src/js/aui/messages';

describe('aui/messages', function () {
    var messagebar;
    var clock;
    var closeableMessage;

    beforeEach(function () {
        $('#test-fixture').html('<div id="aui-message-bar"></div>');
        messagebar = $('#aui-message-bar');
        clock = sinon.useFakeTimers();
        closeableMessage = createMessageWithID('close-message-test');
    });

    afterEach(function () {
        clock.restore();
        $('.aui-message').remove();
    });

    function pressSpace () {
        helpers.pressKey(keyCode.SPACE);
    }

    function pressEnter () {
        helpers.pressKey(keyCode.ENTER);
    }

    function createMessageWithID (testid) {
        createMessageWithIDAndSetCloseable(testid, true);
    }

    function createMessageWithIDAndSetCloseable (testid, closeable) {
        messages.info({
            id: testid,
            title: 'Title',
            body: 'This message was created by messagesSetup() with id ' + testid,
            closeable: closeable
        });
    }

    function checkNoID (target) {
        return {
            found: target.find('.aui-message')[0].getAttribute('id'),
            expected: null
        };
    }

    it('globals', function () {
        expect(AJS).to.contain({
            messages: messages
        });
    });

    it('Messages API', function () {
        expect(messages).to.be.an('object');
        expect(messages.setup).to.be.a('function');
        expect(messages.makeCloseable).to.be.a('function');
        expect(messages.template).to.be.a('string');
        expect(messages.createMessage).to.be.a('function');
    });

    it('Messages ID test: bad ID', function () {
        $('.aui-message').remove();
        createMessageWithID('#t.e.st-m### e s s a \'\'\'\"\"g e-id-full-of-dodgy-crap');
        var checkedNoID = checkNoID(messagebar);
        expect(checkedNoID.found).to.equal(checkedNoID.expected);
    });

    it('Messages ID test: no ID', function () {
        $('.aui-message').remove();
        createMessageWithID();
        var checkedNoID = checkNoID(messagebar);
        expect(checkedNoID.found).to.equal(checkedNoID.expected);
    });

    it('Messages ID test: good ID', function () {
        expect($('#close-message-test').length).to.equal(1);
    });

    it('Closeable messages get a close button', function () {
        expect($('#close-message-test').find('.icon-close').length).to.equal(1);
    });

    it('Closing a message triggers the document aui-close-message event', function () {
        expect($('#close-message-test').length).to.equal(1);

        var closeMessageHandler = sinon.spy();
        $(document).on('aui-message-close', closeMessageHandler);

        $('#close-message-test .icon-close').click();
        clock.tick(100);

        expect($('#close-message-test').length).to.equal(0);
        closeMessageHandler.should.have.been.calledOnce;
    });

    it('Calling makeCloseable on a closeable message will not generate multiple close buttons', function () {
        messages.makeCloseable('#close-message-test');

        expect($('#close-message-test').find('.icon-close').length).to.equal(1);
    });

    it('Pressing SPACE when focused on icon-close will close a message box', function () {
        expect($('#close-message-test').length).to.equal(1);

        var closeMessageHandler = sinon.spy();
        $(document).on('aui-message-close', closeMessageHandler);

        $('#close-message-test .icon-close')[0].focus();
        pressSpace();
        clock.tick(100);

        expect($('#close-message-test').length).to.equal(0);
        closeMessageHandler.should.have.been.calledOnce;
    });

    it('Pressing ENTER when focused on icon-close will close a message box', function () {
        expect($('#close-message-test').length).to.equal(1);

        var closeMessageHandler = sinon.spy();
        $(document).on('aui-message-close', closeMessageHandler);

        $('#close-message-test .icon-close')[0].focus();
        pressEnter();
        clock.tick(100);

        expect($('#close-message-test').length).to.equal(0);
        closeMessageHandler.should.have.been.calledOnce;
    });

    it('Pressing SPACE when NOT focused on icon-close will not close message', function () {
        var testLink = $('<a href="http://www.google.com/" id="test-link">Click Me</a>');
        $('#close-message-test .title').append(testLink);
        expect($('#close-message-test').length).to.equal(1);

        var closeMessageHandler = sinon.spy();
        $(document).on('aui-message-close', closeMessageHandler);

        testLink.focus();
        pressSpace();
        clock.tick(100);

        expect($('#close-message-test').length).to.equal(1);
        closeMessageHandler.should.have.not.been.calledOnce;
    });

    it('Calling makeCloseable() on a non-closeable message will convert it to a closeable message', function () {
        $('.aui-message').remove();
        createMessageWithIDAndSetCloseable('close-message-test', false);
        expect($('#close-message-test').length).to.equal(1);
        expect($('#close-message-test.closeable').length, 0, 'No closeable message present');
        messages.makeCloseable('#close-message-test');
        expect($('.closeable').length, 1, 'Message is now closeable');

        var closeMessageHandler = sinon.spy();
        $(document).on('aui-message-close', closeMessageHandler);

        $('#close-message-test .icon-close').click();
        clock.tick(100);

        expect($('#close-message-test').length).to.equal(0);
        closeMessageHandler.should.have.been.calledOnce;
    });

    it('Messages setup() should enable closeable functionality on messages existing in the DOM', function () {
        $('<div id="markup-message" class="aui-message closeable"><p>Message text</p></div>').appendTo('#test-fixture');
        messages.setup();

        expect($('#markup-message .icon-close').length, 1, 'Close icon added to message');

        var closeMessageHandler = sinon.spy();
        $(document).on('aui-message-close', closeMessageHandler);

        $('#markup-message .icon-close').click();
        clock.tick(100);

        expect($('#markup-message').length).to.equal(0);
        closeMessageHandler.should.have.been.calledOnce;
    });

    it('Messages setup() should enable fadeout functionality on messages existing in the DOM', function () {
        $.fx.off = true; // fadeOut animation causes test to fail. This disables animations, immediately transitioning to end state

        $('<div id="markup-message" class="aui-message fadeout"><p>Message text</p></div>').appendTo('#test-fixture');
        expect($('#markup-message').is(':visible')).to.equal(true);

        messages.setup();
        clock.tick(10000); // Default fadeout delay is 5 seconds + .5 second fadeout duration.
        // Wait ten seconds to ensure message has been removed

        expect($('#markup-message').is(':visible')).to.equal(false);

        $.fx.off = false; // Re-enable animations
    });
});
