import $ from '../../../src/js/aui/jquery';
import FocusManager from '../../../src/js/aui/focus-manager';

describe('aui/focus-manager', function () {
    var $el;
    var spy;
    var $container;
    var $el1;
    var $el2;
    var spy1;
    var spy2;

    function createSingleInput () {
        $el = $('<input type="text" />').appendTo('#test-fixture');
        spy = sinon.spy();
    }

    function createTwoInputs () {
        $container = $('<div></div>').appendTo('#test-fixture');
        $el1 = $('<input type="text" />').appendTo($container);
        $el2 = $('<input type="text" />').appendTo($container);
        spy1 = sinon.spy();
        spy2 = sinon.spy();
    }

    function createSingleInputAndFocus () {
        createSingleInput();
        $el.focus(spy);
    }

    function createTwoInputsAndFocus () {
        createTwoInputs();
        $el1.focus(spy1);
        $el2.focus(spy2);
    }

    function createSingleInputAndBlur () {
        createSingleInput();
        $el.blur(spy);
    }

    function createTwoInputsAndBlur () {
        createTwoInputs();
        $el1.blur(spy1);
        $el2.blur(spy2);
    }

    it('globals', function () {
        expect(AJS).to.contain({
            FocusManager: FocusManager
        });
    });

    it('enter() focuses on the first element only using the default selector', function () {
        createTwoInputsAndFocus();

        new FocusManager().enter($container);

        spy1.should.have.been.calledOnce;
        spy2.should.have.not.been.called;
    });

    it('enter() does not focus if data-aui-focus="false" is provided', function () {
        createTwoInputsAndFocus();

        $container.attr('data-aui-focus', 'false');
        new FocusManager().enter($container);

        spy1.should.have.not.been.called;
        spy2.should.have.not.been.called;
    });

    it('enter() does not focus if data-aui-focus="false" is provided and data-aui-focus-selector is present', function () {
        createTwoInputsAndFocus();
        $el2.attr('id', 'sideshow-bob');
        $container.attr('data-aui-focus', 'false');
        $container.attr('data-aui-focus-selector', '#sideshow-bob');

        new FocusManager().enter($container);

        spy1.should.have.not.been.called;
        spy2.should.have.not.been.called;
    });

    it('enter() focuses on the specified element using a custom selector', function () {
        createTwoInputsAndFocus();
        $el2.attr('id', 'sideshow-bob');
        $container.attr('data-aui-focus-selector', '#sideshow-bob');

        new FocusManager().enter($container);

        spy1.should.have.not.been.called;
        spy2.should.have.been.calledOnce;
    });

    it('enter() focuses on the first element only using a custom selector', function () {
        createTwoInputsAndFocus();
        $el2.attr('id', 'sideshow-bob');
        $container.attr('data-aui-focus-selector', ':input');

        new FocusManager().enter($container);

        spy1.should.have.been.calledOnce;
        spy2.should.have.not.been.called;
    });

    it('enter() selects passed element if it matches the focus selector', function () {
        createSingleInputAndFocus();

        new FocusManager().enter($el);

        spy.should.have.been.calledOnce;
    });

    it('exit() blurs the active element', function () {
        createTwoInputsAndBlur();
        $el1.focus();

        new FocusManager().exit($container);

        spy1.should.have.been.calledOnce;
    });

    it('exit() blurs the active element if the passed element is focussed', function () {
        createSingleInputAndBlur();
        $el.focus();

        new FocusManager().exit($el);

        spy.should.have.been.calledOnce;
    });

    it('exit() does not trigger blur on an element that is not underneath it', function () {
        createTwoInputsAndBlur();
        var $el = $('<input type="text" />').appendTo('#test-fixture');
        $el.focus();

        new FocusManager().exit($container);

        spy1.should.have.not.been.called;
        spy2.should.have.not.been.called;
    });

    it('preserves focus after enter() then exit()', function () {
        createTwoInputsAndBlur();
        var $focusButton = $('<button id="focusButton">Focus button</button>');
        $('#test-fixture').append($focusButton);
        $focusButton.focus();

        new FocusManager().enter($container);
        expect($focusButton.is(document.activeElement)).to.be.false;
        new FocusManager().exit($container);

        expect($focusButton.is(document.activeElement)).to.be.true;
    });

    describe(':aui-focusable', function () {
        it('matches an <input>', function () {
            var html = '<input>';
            var $el = $(html);
            $('#test-fixture').append($el);
            expect($el.is(':aui-focusable')).to.be.true;
        });

        it('does not match an invisible <input>', function () {
            var html = '<input style="visibility: hidden">';
            var $el = $(html);
            $('#test-fixture').append($el);
            expect($el.is(':aui-focusable')).to.be.false;
        });

        it('matches a visible <input> nested inside an invisible <div>', function () {
            var html = '<div style="visibility: hidden"><input id="nested-visible-input" style="visibility: visible"></div>';
            $('#test-fixture').html(html);
            var $el = $('#nested-visible-input');
            expect($el.is(':aui-focusable')).to.be.true;
        });
    });
});
