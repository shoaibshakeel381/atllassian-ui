'use strict';

var commander = require('../lib/commander');
var debug = require('../lib/debug');
var expect = require('../lib/expect');
var galvatron = require('../lib/galvatron');
var gulp = require('gulp');
var Server = require('karma').Server;
var mac = require('mac');
var objectAssign = require('object-assign');

commander
    .option('-b, --browsers [Chrome_1024x768]', 'Browsers to run tests against')
    .option('-c, --coverage', 'Enable code coverage reporting.')
    .option('-g, --grep [pattern]', 'The grep pattern matching the tests you want to run.')
    .option('-l, --logs', 'Enable logging of the browser console.')
    .option('-r, --reporters <reporters>', 'Reporters to use. Comma-separated for multiple.')
    .option('-s, --saucelabs', 'Whether to run tests against saucelabs.')
    .parse(process.argv);

var browsers = (commander.browsers || 'Chrome_1024x768,Firefox_1024x768').split(',');
var clientArgs = [];

var reporters = (commander.reporters || ['progress', 'junit'].join(',')).split(',');

if (commander.coverage && reporters.indexOf('coverage') === -1) {
    reporters.push('coverage');
}

if (commander.grep) {
    clientArgs.push('--grep');
    clientArgs.push(commander.grep);
}
var config = {
    hostname: commander.watch ? '0.0.0.0' : 'localhost',
    autoWatch: !!commander.watch,
    singleRun: !commander.watch,
    frameworks: ['mocha', 'sinon-chai'],
    browsers: browsers,
    customLaunchers: {
        // jscs:disable requireCamelCaseOrUpperCaseIdentifiers
        Chrome_1024x768:{
            base: 'Chrome',
            flags: ['--window-size=1024,768']
        },
        Firefox_1024x768:{
            base: 'Firefox',
            flags: ['-foreground', '-width', '1024', '-height', '768']
        }
        // jscs:enable requireCamelCaseOrUpperCaseIdentifiers
    },
    reporters: reporters,
    client: {
        args: clientArgs,
        captureConsole: !!commander.logs
    },
    coverageReporter: {
        dir: 'reports/istanbul',
        type: 'html'
    },
    junitReporter: {
        outputDir: 'tests',
        suite: '',
        useBrowserName: true
    },
    preprocessors: {
        'src/less/{**/*,*}.less': 'less'
    },
    lessPreprocessor: {
        options: {
            paths: ['src/less/**'],
            save: false,
            relativeUrls: true
        }
    },
    files: [
        'src/less/batch/aui-experimental.less',
        'src/less/batch/aui.less',
        'tests/styles/all.css',
        'bower_components/aui/tests/styles/all.css', // Finds the file when running tests from the aui-adg repo.

        // Depending on which jQuery version has been installed by Bower the
        // dist file might be in a different location. Since we should only
        // have one version of jQuery installed at a given time, only one
        // of these paths should resolve.
        'bower_components/jquery/jquery.js',
        'bower_components/jquery/dist/jquery.js',

        'bower_components/jquery-migrate/jquery-migrate.js',
        '.tmp/unit.js'
    ]
};

if (commander.saucelabs) {
    var saucelabsLaunchers = require('../lib/saucelabs-launchers');
    config = objectAssign(config, {
        sauceLabs: {
            testName: 'AUI unit tests (master)',
            recordScreenshots: false,
            connectOptions: {
                verbose: true,
                verboseDebugging: true
            }
        },
        customLaunchers: saucelabsLaunchers,
        browsers: Object.keys(saucelabsLaunchers),
        captureTimeout: 120000,
        reporters: ['saucelabs', 'dots', 'junit'],
        autoWatch: false,
        singleRun: true,
        concurrency: 5,
        client: {
            captureConsole: false
        }
    });
    delete config.hostname;
}

function run () {
    var server = new Server(config);
    server.start()
}
module.exports = mac.series(
    function () {
        var bundle = galvatron.bundle('tests/unit.js');
        return gulp.src(bundle.files)
            .pipe(bundle.watchIf(commander.watch))
            .pipe(debug({title: 'test'}))
            .pipe(bundle.stream())
            .pipe(gulp.dest('.tmp'))
            .pipe(expect(bundle.files));
    },
    run
);
