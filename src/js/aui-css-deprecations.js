'use strict';

import { css } from './aui/internal/deprecation';
import amdify from './aui/internal/amdify';

//Pre 5.9.0 Deprecations
function deprecateSincePreFiveNineZero(classes, displayName, alternativeName) {
    var options = {};
    options.displayName = displayName;
    if (alternativeName) {
        options.alternativeName = alternativeName;
    }
    css(classes, options);
}

deprecateSincePreFiveNineZero('.aui-dropdown2-trigger.aui-style-dropdown2triggerlegacy1', 'Dropdown2 legacy trigger');
deprecateSincePreFiveNineZero('.aui-message span.aui-icon', 'Message icon span');
deprecateSincePreFiveNineZero('.aui-nav-pagination > li.aui-nav-current', '', 'aui-nav-selected');
deprecateSincePreFiveNineZero('.aui-tabs.vertical-tabs', 'Vertical tabs');
deprecateSincePreFiveNineZero('form.aui span.content', 'unprefixed aui span content');

deprecateSincePreFiveNineZero([
    'form.aui .button',
    'form.aui .buttons-container'
], 'Unprefixed buttons', 'aui-button and aui-buttons');

deprecateSincePreFiveNineZero([
    'form.aui .icon-date',
    'form.aui .icon-range',
    'form.aui .icon-help',
    'form.aui .icon-required',
    'form.aui .icon-inline-help',
    'form.aui .icon-users',
    '.aui-icon-date',
    '.aui-icon-range',
    '.aui-icon-help',
    '.aui-icon-required',
    '.aui-icon-users',
    '.aui-icon-inline-help'
], 'Form icons');

deprecateSincePreFiveNineZero([
    '.aui-icon.icon-move-d',
    '.aui-icon.icon-move',
    '.aui-icon.icon-dropdown-d',
    '.aui-icon.icon-dropdown',
    '.aui-icon.icon-dropdown-active-d',
    '.aui-icon.icon-dropdown-active',
    '.aui-icon.icon-minimize-d',
    '.aui-icon.icon-minimize',
    '.aui-icon.icon-maximize-d',
    '.aui-icon.icon-maximize'
], 'Core icons');
deprecateSincePreFiveNineZero([
    '.aui-message.error',
    '.aui-message.warning',
    '.aui-message.hint',
    '.aui-message.info',
    '.aui-message.success'
],  'Unprefixed message types AUI-2150');

deprecateSincePreFiveNineZero([
    '.aui-dropdown2 .active',
    '.aui-dropdown2 .checked',
    '.aui-dropdown2 .disabled',
    '.aui-dropdown2 .interactive'
], 'Unprefixed dropdown2 css AUI-2150');

deprecateSincePreFiveNineZero([
    '.aui-page-header-marketing',
    '.aui-page-header-hero'
], 'Marketing style headings');

// 5.9.0
// -----


function deprecateSinceFiveNineZero(classes, displayName, alternativeName) {
    var options = {};
    options.sinceVersion = '5.9.0';
    options.removeVersion = '6.0.0';
    options.displayName = displayName;
    options.alternativeName = alternativeName;
    css(classes, options);
}
deprecateSinceFiveNineZero('.aui-badge', 'AUI Badges class', '<aui-badge> web component');

deprecateSinceFiveNineZero('.arrow', 'Inline dialog arrow','aui-inline-dialog-arrow');
deprecateSinceFiveNineZero('.contents', 'Inline dialog contents', 'aui-inline-dialog-contents');

deprecateSinceFiveNineZero('.error', 'Messages error', 'aui-message-error');
deprecateSinceFiveNineZero('.generic', 'Messages generic', 'aui-message-generic');
deprecateSinceFiveNineZero('.hint', 'Messages hint', 'aui-message-hint');
deprecateSinceFiveNineZero('.info', 'Messages info', 'aui-message-info');
deprecateSinceFiveNineZero('.success', 'Messages success', 'aui-message-success');
deprecateSinceFiveNineZero('.warning', 'Messages warning', 'aui-message-warning');


// 5.10.0
function deprecateSinceFiveTenZero(classes, displayName, alternativeName) {
    var options = {};
    options.sinceVersion = '5.10.0';
    options.removeVersion = '6.0.0';
    options.displayName = displayName;
    options.alternativeName = alternativeName;
    css(classes, options);
}

deprecateSinceFiveTenZero('.aui-lozenge', 'AUI Lozenge', '<aui-lozenge> web component');

amdify('aui/css-deprecation-warnings');
