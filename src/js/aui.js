'use strict';

import './aui/polyfills/console';
import './aui/polyfills/custom-event';

import './aui/binders/placeholder';

import './jquery/jquery.os';
import './jquery/jquery.moveto';

import './aui/cookie';
import './aui/dialog';
import './aui/event';
import './aui/events';
import './aui/forms';
import './aui/internal/log';
import './aui/blanket';
import './aui/firebug';
import './aui/internal/add-id';
import './aui/alphanum';
import './aui/binder';
import './aui/clone';
import './aui/contain-dropdown';
import './aui/contains';
import './aui/draw-logo';
import './aui/drop-down';
import './aui/dropdown2';
import './aui/escape';
import './aui/escape-html';
import './aui/filter-by-search';
import './aui/focus-manager';
import './aui/format';
import './aui/i18n';
import './aui/unique-id';
import './aui/include';
import './aui/index-of';
import './aui/inline-dialog';
import './aui/inline-dialog2';
import './aui/is-clipped';
import './aui/is-visible';
import './aui/key-code';
import './aui/layer';
import './aui/layer-manager';
import './aui/messages';
import './aui/navigation';
import './aui/on-text-resize';
import './aui/populate-parameters';
import './aui/prevent-default';
import './aui/header';
import './aui/set-current';
import './aui/set-visible';
import './aui/stop-event';
import './aui/tabs';
import './aui/template';
import './aui/toggle-class-name';
import './aui/to-init';
import './aui/unbind-text-resize';
import './aui/when-i-type';

import './aui/setup';

export default window.AJS;
