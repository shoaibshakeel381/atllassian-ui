'use strict';

import hljs from 'highlight.js';
import skate from 'skatejs';

function setupCodeBlockContents(element) {
    const pre = document.createElement('pre');
    element.innerHTML = '';
    element.appendChild(pre);
}

function getIndentLength(str) {
    if (str) {
        return str.match(/^\s*/)[0].length;
    }
}

function getLang (element) {
    const type = element.getAttribute('type');
    return type.split('/')[1];
}

function setIndentLength (len) {
    return len > 0 ? new Array(len + 1).join(' ') : '';
}

/**
 * A web component that renders javascript, html and other languages as a code listing using highlightjs
 *
 * Example:
 * <noscript is="aui-docs-code" type="html">
 *  <button class="aui-button aui-button-primary">Button</button>
 * </noscript>
 *
 * # Attributes
 *  - type: a highlighting language to use, can be one of `text/css`, `text/handlebars` (sometimes used instead of soy), `text/js`, `text/html`.
 *          **Note:** `text/javascript` cannot be used, or the javascript will be executed. Use `text/js` instead.
 *  - lines: (boolean attribute) whether or not to include line numbers in the output.
 */
export default skate('aui-docs-code', {
    // We extend <noscript> since it is the only element that will preserve the
    // raw contents of the custom element (e.g., avoids boolean attributes from
    // being expanded into foo="") and we're able to avoid skating the example
    // contents for code listings, and it also does not block when the browser
    // encounters it.
    extends: 'noscript',
    created: function (element) {
        const oldElement = element;
        const rawHtml = element.innerHTML;

        const lang = getLang(element);
        const lines = rawHtml.split('\n');
        const showLines = element.hasAttribute('lines') && element.getAttribute('lines') !== 'false';

        if (lang === 'javascript') {
            console.error('To avoid JavaScript evaluation by the browser, noscript[is="aui-docs-code"] elements must not have type="text/javascript".');
        }

        const wrapper = document.createElement('div');
        wrapper.className = 'aui-code-block-wrapper';
        element = document.createElement('aui-code-block');

        // Trim leading empty lines.
        if (!lines[0].trim()) {
            lines.splice(0, 1);
        }

        // Trim trailing empty lines
        if (!lines[lines.length - 1].trim()) {
            lines.splice(lines.length - 1, 1);
        }

        const baseIndent = getIndentLength(lines[0]);

        setupCodeBlockContents(element);
        const pre = element.querySelector('pre');

        lines.forEach(function (line, index) {
            const indent = getIndentLength(line) - baseIndent;
            const num = document.createElement('code');
            const code = document.createElement('code');
            const nl = document.createTextNode('\n');

            line = line.trim();
            line = line.replace(/&gt;/g, '>');
            line = line.replace(/&lt;/g, '<');

            num.className = 'aui-docs-code-line-number';
            num.innerHTML = index + 1;
            code.className = 'aui-docs-code-line-content';
            code.innerHTML = setIndentLength(indent) + hljs.highlight(lang || 'html', line).value;

            if (showLines) {
                pre.appendChild(num);
            }

            pre.appendChild(code);
            pre.appendChild(nl);
        });

        wrapper.appendChild(element);

        if (oldElement) {
            oldElement.parentNode.insertBefore(wrapper, oldElement);
        }
    }
});
