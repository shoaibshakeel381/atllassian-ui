import skate from 'skatejs';
import '../../../../src/js/aui/dropdown2';
import '../../../../src/js/aui-soy';

var template = function (idPrefix) {
    return {
        plainSection: aui.dropdown2.itemGroup({
            id: idPrefix + '-link-section',
            items: [
                {text: 'Menu item', id: idPrefix + 'item1', href: '#link'},
                {text: 'Menu item', id: idPrefix + 'item2'},
                {text: 'Menu item', id: idPrefix + 'item3'},
                {text: 'Menu item', id: idPrefix + 'item4'}
            ]
        }),

        plainSection2: aui.dropdown2.itemGroup({
            id: idPrefix + '-link-section-2',
            items: [
                {text: 'Menu item', id: idPrefix + 'item21'},
                {text: 'Menu item', id: idPrefix + 'item22'},
                {text: 'Menu item', id: idPrefix + 'item23'},
                {text: 'Menu item', id: idPrefix + 'item24'}
            ]
        }),

        hiddenSection: aui.dropdown2.itemGroup({
            id: idPrefix + '-hidden-section',
            items: [
                {itemType: 'checkbox', text: 'Menu item', id: idPrefix + 'hidden1-unchecked-disabled', isInteractive: true, isDisabled: true, isHidden: true},
                {itemType: 'checkbox', text: 'Menu item', id: idPrefix + 'hidden2-checked', isInteractive: true, isChecked: true, isHidden: true}
            ]
        }),

        interactiveSection: aui.dropdown2.itemGroup({
            id: idPrefix + '-interactive-section',
            items: [
                {itemType: 'radio', text: 'Menu item', id: idPrefix + 'iradio1-interactive-checked', isInteractive: true, isChecked: true},
                {itemType: 'radio', text: 'Menu item', id: idPrefix + 'iradio2-interactive-unchecked', isInteractive: true},
                {itemType: 'radio', text: 'Menu item', id: idPrefix + 'iradio3-unchecked'}
            ]
        }),

        radioSection: aui.dropdown2.itemGroup({
            id: idPrefix + '-radio-section',
            items: [
                {itemType: 'radio', text: 'Menu item', id: idPrefix + 'radio1-unchecked', isInteractive: true},
                {itemType: 'radio', text: 'Menu item', id: idPrefix + 'radio2-checked', isInteractive: true, isChecked: true},
                {itemType: 'radio', text: 'Menu item', id: idPrefix + 'radio3-unchecked', isInteractive: true}
            ]
        }),

        checkboxSection: aui.dropdown2.itemGroup({
            id: idPrefix + '-checkbox-section',
            items: [
                {itemType: 'checkbox', text: 'Menu item', id: idPrefix + 'check1-unchecked', isInteractive: true},
                {itemType: 'checkbox', text: 'Menu item', id: idPrefix + 'check2-checked', isInteractive: true, isChecked: true},
                {itemType: 'checkbox', text: 'Menu item', id: idPrefix + 'check3-unchecked', isInteractive: true}
            ]
        })
    };
};

var AccessibleDropdown = function () {
    var idPrefix = AJS.id('test-accessible-dropdown');
    var triggerId = idPrefix + '-trigger';
    var menuId = idPrefix + '-menu';


    var $trigger = AJS.$(aui.dropdown2.trigger({
        id: triggerId,
        text: 'Example dropdown',
        menu: {
            id: menuId
        }
    }));

    var $dropdown = AJS.$(aui.dropdown2.contents({
        id: menuId
    }));

    var testComponent = {
        getItem: function (index, section) {
            var $itemContainer = section ? this.$dropdown.find('.aui-dropdown2-section').eq(section - 1) : this.$dropdown;
            return $itemContainer.find('li a, li span').eq(index - 1);
        },

        $dropdown: $dropdown,

        addTrigger: function () {
            this.$trigger = $trigger;
        },

        initialise: function ($parent) {
            this.$dropdown.appendTo($parent || AJS.$('#test-fixture'));
            skate.init(this.$dropdown[0]);

            if (this.$trigger && this.$trigger.length) {
                this.$trigger.appendTo($parent || AJS.$('#test-fixture'));
                skate.init(this.$trigger[0]);
            }
        },

        addPlainSection: function () {
            $('#' + idPrefix + '-link-section').remove();
            $dropdown.children('div').append(
                template(idPrefix).plainSection
            );
        },

        addPlainSection2: function () {
            $('#' + idPrefix + '-link-section-2').remove();
            $dropdown.children('div').append(
                template(idPrefix).plainSection2
            );
        },

        addHiddenSection: function () {
            var id = idPrefix + '-hidden-section';
            $('#' + id).remove();
            $dropdown.children('div').append(
                template(idPrefix).hiddenSection
            );
        },

        addInteractiveSection: function () {
            var id = idPrefix + '-interactive-section';
            $('#' + id).remove();

            $dropdown.children('div').append(
                template(idPrefix).interactiveSection
            );
        },

        addRadioSection: function () {
            var id = idPrefix + '-radio-section';
            $('#' + id).remove();

            $dropdown.children('div').append(
                template(idPrefix).radioSection
            );
        },

        addCheckboxSection: function () {
            var id = idPrefix + '-checkbox-section';
            $('#' + id).remove();
            $dropdown.children('div').append(
                template(idPrefix).checkboxSection
            );
        },

        addSubmenuSection: function (dropdownToAssociate) {
            var submenuId = dropdownToAssociate.$dropdown[0].id;

            $dropdown.children('div').append(aui.dropdown2.itemGroup({
                items: [
                    {text: 'Dummy item 1', id: idPrefix + 'dd2-menu-1-child-1'},
                    {text: 'Open submenu level 1', submenuTarget: submenuId, id: idPrefix + 'dd2-menu-1-child-2'}
                ]
            }));
        },

        checkEvent: 'aui-dropdown2-item-check',
        uncheckEvent: 'aui-dropdown2-item-uncheck',

        isChecked: function (el) {
            return $(el).is('[aria-checked="true"].checked.aui-dropdown2-checked');
        },

        isUnchecked: function (el) {
            var $el = $(el);
            return !$el.is('.checked, .aui-dropdown2-checked') && $el.is('[aria-checked="false"]');
        }
    };

    return testComponent;
};

export default AccessibleDropdown;
