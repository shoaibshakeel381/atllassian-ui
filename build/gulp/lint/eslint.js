'use strict';

var gulp = require('gulp');
var eslint = require('gulp-eslint');

module.exports = function () {
    return gulp.src([
        'gulpfile.js',
        'src/js/**/*.js',
        'tests/**/*.js'
    ])
        .pipe(eslint())
        .pipe(eslint.format());
};
