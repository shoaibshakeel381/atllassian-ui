'use strict';

var commander = require('../../lib/commander');
var debug = require('../../lib/debug');
var expect = require('../../lib/expect');
var fs = require('fs-extra');
var gulp = require('gulp');
var gulpLess = require('gulp-less');
var gulpMinifyCss = require('gulp-minify-css');
var gulpRename = require('gulp-rename');
var gulpWatchLess = require('gulp-watch-less');
var mac = require('mac');
var path = require('path');
var streamIf = require('../../lib/stream-if');

function cleanup () {
    fs.removeSync('dist/aui/css');
    fs.mkdirsSync('dist/aui/css');
    fs.mkdirsSync('.tmp/dist/aui/css');
}

function mainImages () {
    return gulp.src('src/less/images/**/*')
        .pipe(gulp.dest('dist/aui/css/images'))
        .pipe(gulp.dest('.tmp/dist/aui/css/images'));
}

function select2Images () {
    return gulp.src([
        'src/css-vendor/jquery/plugins/*.png',
        'src/css-vendor/jquery/plugins/*.gif'
    ])
        .pipe(gulp.dest('dist/aui/css'))
        .pipe(gulp.dest('.tmp/dist/aui/css'));
}

function lessFile (fileName) {
    var src = 'src/less/batch/' + fileName + '.less';
    return gulp.src(src)
        .pipe(debug({title: 'dist/less/compile'}))
        .pipe(streamIf(commander.watch, gulpWatchLess.bind(null, src)))
        .pipe(gulpLess({
            paths: [
                path.basename(src)
            ]
        }))
        .pipe(gulp.dest('dist/aui/css'))
        .pipe(gulp.dest('.tmp/dist/aui/css'))
        .pipe(debug({title: 'dist/less/optimise'}))
        .pipe(gulpMinifyCss())
        .pipe(gulpRename({
            basename: fileName,
            suffix: '.min'
        }))
        .pipe(gulp.dest('dist/aui/css'))
        .pipe(gulp.dest('.tmp/dist/aui/css'))
        .pipe(expect(src));
}

module.exports = mac.series(
    cleanup,
    mac.parallel(
        mainImages,
        select2Images,
        mac.parallel(
            lessFile.bind(null, 'aui'),
            lessFile.bind(null, 'aui-experimental')
        )
    )
);
