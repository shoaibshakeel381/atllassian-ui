var gulp = require('gulp');
var sourcemaps = require('gulp-sourcemaps');
var less = require('./less');
var mac = require('mac');
var path = require('path');
var streamIf = require('./stream-if');

function mainImages (sourcePath) {
    return gulp.src(path.join(sourcePath, 'src/less/images/**'))
        .pipe(gulp.dest('lib/css/images'));
}

function fonts (sourcePath) {
    return gulp.src(path.join(sourcePath, 'src/less/fonts/**'))
        .pipe(gulp.dest('lib/css/fonts'));
}

function select2Images (sourcePath) {
    return gulp.src([
        path.join(sourcePath, 'src/css-vendor/jquery/plugins/*.png'),
        path.join(sourcePath, 'src/css-vendor/jquery/plugins/*.gif')
    ]).pipe(gulp.dest('lib/css'));
}

function lessFiles (sourcePath) {
    var files = path.join(sourcePath, 'src/less/**.less');

    return gulp.src(files)
        .pipe(sourcemaps.init())
        .pipe(less(files))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('lib/css'));
}

module.exports = function libLess (sourcePath) {
    sourcePath = sourcePath || './';
    return mac.parallel(
        mainImages(sourcePath),
        fonts(sourcePath),
        select2Images(sourcePath),
        lessFiles(sourcePath)
    )
};
